var express = require("express");
var axios = require("axios");
var router = express.Router();

var _ = require("lodash");

router.post("/getResult", async function(req, res) {
  const time = req.body.time;
  const userId = "test1";
  if (!time) return res.status(400).json({ error: "time is null" });
  if (!userId) return res.status(400).json({ error: "userid is null" });
  try {
    var result = await axios.post("http://localhost:5005/api/point", {
      time: time,
      userId: userId
    });
    return res.status(200).json(result.data);
  } catch (e) {
    return res.status(500).json(e.message);
  }
});

module.exports = router;
