(function($, Timer) {
  var timer = new Timer();
  $("#simulateBtn").on("click", function(e) {
    $("#simulateBtn").css("display", "none");
    $("#stopBtn").css("display", "block");
    timer.start({ precision: "secondTenths" });
  });

  $("#stopBtn").on("click", function(e) {
    $("#simulateBtn").css("display", "block");
    $("#stopBtn").css("display", "none");
    const totalTime = timer.getTotalTimeValues().seconds;
    timer.stop();
    $.ajax({
      url: "/api/tap/getResult",
      method: "POST",
      data: { time: totalTime },
      dataType: "json"
    })
      .done(function(r) {
        console.log(r.data);
        $("#result").html(JSON.stringify(r.data));
      })
      .fail(function(e) {
        console.log("request fail");
      });
  });

  timer.addEventListener("secondTenthsUpdated", function(e) {
    $("#basicUsage").html(
      timer
        .getTimeValues()
        .toString(["hours", "minutes", "seconds", "secondTenths"])
    );
  });

  timer.addEventListener("stop", function(e) {
    console.log(timer.getTimeValues());
  });
})($, easytimer.Timer);
