var express = require("express");
var _ = require("lodash");
var rules = require("../constant/rules.json");
var router = express.Router();
var users = [];

router
  .get("/", function(req, res) {
    const userId = req.query.userId;
    const user = _.find(users, { userId: userId });
    if (!user)
      res.status(204).json({
        error: "not found user"
      });
    else
      res.status(200).json({
        totalPoints: user.points,
        userId: user.userId,
        product: user.products ? user.products : []
      });
  })
  .post("/", function(req, res) {
    const time = req.body.time;
    const userId = req.body.userId;
    if (!time) return res.status(400).json({ error: "time is null" });
    if (!userId) return res.status(400).json({ error: "userid is null" });
    const rule = _.find(rules, e => {
      if (e.timeRange.min <= time && e.timeRange.max >= time) return e;
    });
    if (!rule)
      return res.status(204).json({
        data: {
          message: "cannot found rule"
        }
      });
    const user = _.find(users, { userId: userId });
    if (!user) {
      users.push({
        userId,
        points: rule.points,
        history: [rule]
      });
      return res.status(200).json({
        data: {
          points: rule.points,
          userId: userId,
          level: rule.level,
          totalPoints: rule.points
        }
      });
    } else {
      user.points = user.points + rule.points;
      user.history.push(rule);
      return res.status(200).json({
        data: {
          points: rule.points,
          userId: userId,
          level: rule.level,
          totalPoints: user.points
        }
      });
    }
  });

module.exports = {
  router,
  users
};
