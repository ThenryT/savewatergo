var express = require("express");
var _ = require("lodash");
var products = require("../constant/product");
const { users } = require("./point");
var router = express.Router();

router
  .get("/", function(req, res) {
    res.status(200).json(products);
  })
  .get("/:userId", function(req, res) {
    const userId = req.query.userId;

    if (!userId) {
      errors.push("userId is empty");
    }

    const user = _.find(users, { userId: userId });
    if (!user) {
      return res.json(400).json("you are not register");
    }

    return res.status(200).json(user);
  })
  .post("/", function(req, res) {
    const productId = req.body.productId;
    const userId = req.body.userId;
    console.log(productId, userId);
    const errors = [];
    if (!productId) {
      errors.push("productId is empty");
    }

    if (!userId) {
      errors.push("userId is empty");
    }

    if (errors.length > 0) {
      return res.status(500).json({
        errors
      });
    }

    const user = _.find(users, { userId: userId });
    if (!user) {
      return res.status(400).json("you are not register");
    }

    const product = _.find(products, { id: productId });
    if (!product) {
      return res.status(400).json("product not found");
    }

    if (user.points - product.point < 0) {
      return res.status(200).json({
        success: false,
        message: "you do not have enough point"
      });
    }

    user.points = user.points - product.point;

    if (user.products) {
      user.products.push(product);
    } else {
      user.products = [];
      user.products.push(product);
    }

    return res.status(201).json({
      success: true
    });
  });

module.exports = router;
