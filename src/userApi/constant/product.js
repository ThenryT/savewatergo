module.exports = [
  {
    id: 1,
    title: "1 regular coffee",
    description: "coffee",
    point: 20,
    image: "/coffee-redeem.jpg"
  },
  {
    id: 2,
    title: "1 Brunch",
    point: 200,
    image: "/brunch-redeem.jpg",
    description: ""
  },
  {
    id: 3,
    title: "donate dog",
    point: 30,
    image: "/dog-donate-redeem.jpg",
    description: "worth $5"
  }
];
