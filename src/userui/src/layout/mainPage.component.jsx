import React from "react";
import { Layout, Menu, Icon } from "antd";
import { Switch, Route } from "react-router-dom";
import MyProfile from "../components/myProfile.component";
import RedeemPoint from "../components/redeemPoint.component";
import { Link } from "react-router-dom";
import HowToComponent from "../components/howto.component";

const { Header, Content, Footer, Sider } = Layout;

export default class MainPage extends React.Component {
  render() {
    return (
      <Layout style={{ height: "100%" }}>
        <Sider
          breakpoint="lg"
          collapsedWidth="0"
          onBreakpoint={broken => {
            console.log(broken);
          }}
          onCollapse={(collapsed, type) => {
            console.log(collapsed, type);
          }}
        >
          <div className="logo"></div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["0"]}>
            <Menu.Item key="0">
              <Link to="/user/profile">
                <Icon type="user" />
                <span className="nav-text">My Profile</span>
              </Link>
            </Menu.Item>

            <Menu.Item key="1">
              <Link to="/redeempoint">
                <Icon type="shopping" />
                <span className="nav-text">Redeem your point</span>
              </Link>
            </Menu.Item>

            <Menu.Item key="2">
              <Link to="/howtoredeem">
                <Icon type="book" />
                <span className="nav-text">How to redeem</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: "#fff", padding: 0 }}></Header>
          <Content style={{ margin: "24px 16px 0" }}>
            <Switch>
              <Route exact path={`/user/profile`} component={MyProfile} />
              <Route exact path={`/redeempoint`} component={RedeemPoint} />
              <Route exact path={`/howtoredeem`} component={HowToComponent} />
            </Switch>
          </Content>
          <Footer style={{ textAlign: "center" }}></Footer>
        </Layout>
      </Layout>
    );
  }
}
