import React from "react";
import { Card, Col, Row, Button, notification } from "antd";
import axios from "axios";

const { Meta } = Card;

const openNotification = (message, description, type) => {
  notification[type]({
    message,
    description
  });
};

export default class RedeemPoint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  async componentDidMount() {
    const result = await axios.get(`http://localhost:5005/api/products`);
    console.log(result);
    this.setState({
      items: result.data
    });
  }

  redeem = async productId => {
    try {
      const result = await axios.post("http://localhost:5005/api/products", {
        userId: "test1",
        productId: productId
      });
      console.log(result);
      if (result.data.success) {
        openNotification("Success", result.data.message, "success");
      } else {
        openNotification("Warning", result.data.message, "warning");
      }
    } catch (error) {
      console.error(error);
      openNotification("Error", error.message, "error");
    }
  };

  render() {
    return (
      <Row gutter={16}>
        {this.state.items.map((item, index) => {
          return (
            <Col span={8} key={index}>
              <Card cover={<img alt={item.title} src={item.image} />}>
                <Meta
                  title={item.title}
                  description={
                    <div>
                      <p>
                        <b>{item.point} points</b>
                        <br />
                        {item.description}
                      </p>
                      <Button
                        type="primary"
                        onClick={() => this.redeem(item.id)}
                      >
                        Redeem
                      </Button>
                    </div>
                  }
                />
              </Card>
            </Col>
          );
        })}
      </Row>
    );
  }
}
