import React from "react";
import { Card, Row, Col } from "antd";
import axios from "axios";

const { Meta } = Card;
export default class MyProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "ruby",
      points: 0,
      products: []
    };
  }

  async componentDidMount() {
    const id = "test1";
    const result = await axios.get(
      `http://localhost:5005/api/point?userId=${id}`
    );
    this.setState({
      points: result.data.totalPoints ? result.data.totalPoints : 0,
      products: result.data.product ? result.data.product : []
    });
  }

  render() {
    return (
      <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
        <Card title={this.state.name} extra={<a href="#">Refresh</a>}>
          <div
            style={{
              fontSize: 14,
              color: "rgba(0, 0, 0, 0.85)",
              marginBottom: 16,
              fontWeight: 500
            }}
          >
            <Card title="Points">
              <p>{this.state.points}</p>
            </Card>

            <Card title="Your coupon">
              <Row gutter={16}>
                {this.state.products.map((item, index) => {
                  return (
                    <Col span={8} key={index}>
                      <Card cover={<img alt={item.title} src={item.image} />}>
                        <Meta
                          title={item.title}
                          description={<div>{item.description}</div>}
                        />
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </Card>
          </div>
        </Card>
      </div>
    );
  }
}
