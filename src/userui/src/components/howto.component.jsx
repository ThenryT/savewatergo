import React from "react";
import { Row, Card } from "antd";

export default class HowToComponent extends React.Component {
  render() {
    return (
      <Row>
        <Card title="How to redeem">
          <ul>
            <li>
              check how many points you have in your account (user profile).
            </li>
            <li>
              click redeem your points page and select the product you want to
              redeem.
            </li>
            <li>lastly, click redeem button, ta—da—all done.</li>
            <li> questions, feel free to contact us at info@savewatergo.com</li>
          </ul>
        </Card>
      </Row>
    );
  }
}
