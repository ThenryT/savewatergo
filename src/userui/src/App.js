import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import MainPage from "./layout/mainPage.component";

import "antd/dist/antd.css";
import "./App.css";

function App() {
  return (
    <Router>
      <Route path="/" component={MainPage} />
      <Redirect exact path="/" to="/user/profile" />
    </Router>
  );
}

export default App;
