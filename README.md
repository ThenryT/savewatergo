# SavewaterGo!! prototype 

This is a prototype project used to present the idea of using game to educate studetns to save water.

## Context

Saving water is everyone's obligation. Traditional education ways such as watching vedio or read book cannot essentially change the behaviour of using water.
That's why we plan to create a game to training the children's behaviour to save water. 

## Idea
When chirldren using tap, bathroom or water machine, these stuff will caculate the time and usage of water and sent the data into cloud server
and the server will caculate the point for each users. And user can using App to check their points and using these points to get some digital 
toys and shown in the apps (such as PokerMon Go)

### Architecture

![Image of Architecture](./document/savewater.png)

### Technologies
 * node.js
 * express.js
 * react.js
 * microservice structure
 * antd framework for react
 
## Todo
 * docker setup
 * k8s deployment
 * message queue sturcture
 * hardware setup (tap sensor)
 * test



